@include("layouts.blog.htmlhead")
@include("layouts.blog.navbar")

    <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">The Bootstrap Blog</h1>
        <p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">

          @foreach ($items as $item)
            @component("layouts.blog.post" ,
              [
              "title" => $item->title,
              "text" => $item->text,
              "creation" =>$item->created_at->diffForHumans()
              ]
            )

            @endcomponent
          @endforeach


          <nav>
            <ul class="pager">
              <li><a href="#">Previous</a></li>
              <li><a href="#">Next</a></li>
            </ul>
          </nav>

        </div><!-- /.blog-main -->
        @include("layouts.blog.sidebar")
      </div><!-- /.row -->

    </div><!-- /.container -->

@include("layouts.blog.footer")
