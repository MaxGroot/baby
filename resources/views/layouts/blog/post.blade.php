
          <div class="blog-post">
            <h2 class="blog-post-title">
                {{ $title }}
            </h2>
            <p class="blog-post-meta">{{ $creation }}, 2014 by <a href="#">Mark</a></p>

              {{ $text }}

          </div><!-- /.blog-post -->
