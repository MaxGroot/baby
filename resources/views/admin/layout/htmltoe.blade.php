
<script src="{{ asset('admin/js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/js/chart.min.js') }}"></script>
<script src="{{ asset('admin/js/chart-data.js') }}"></script>
<script src="{{ asset('admin/js/easypiechart.js') }}"></script>
<script src="{{ asset('admin/js/easypiechart-data.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('admin/js/custom.js') }}"></script>
{{ $slot }}
</body>
</html>
