
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ auth()->user()->name }}</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>

		<ul class="nav menu">
			<li><a href="/admin/"><em class="fa fa-home">&nbsp;</em> Home </a></li>
			<li><a href="/admin/settings/"><em class="fa fa-cog">&nbsp;</em> Instellingen</a></li>
			<li><a href="/admin/news/"><em class="fa fa-newspaper-o">&nbsp;</em> Nieuws </a></li>
			<li><a href="/admin/users/"><em class="fa fa-user">&nbsp;</em> Gebruikers </a></li>
			<li><a href="/admin/pages/"><em class="fa fa-file-text">&nbsp; </em> Pagina's </a></li>
			<li><a href="/admin/logout"><em class="fa fa-power-off">&nbsp;</em> Uitloggen </a></li>
		</ul>
	</div><!--/.sidebar-->
