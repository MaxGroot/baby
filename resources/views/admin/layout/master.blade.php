@component("admin.layout.htmlhead")
  @slot("title")
    {{ $title }}
  @endslot
@endcomponent

@component("admin.layout.topbar")
@endcomponent

@component("admin.layout.sidebar")
@endcomponent

@component("admin.layout.main" , [
"title" => $title,
"pageroute" => $pageroute,
"content" => $content

])
@endcomponent

@component("admin.layout.htmltoe")
  @if (!empty($optionalscripts))
    {{ $optionalscripts }}
  @endif
@endcomponent
