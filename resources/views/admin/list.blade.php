@component("admin.layout.master")
  @slot("title")
    Overzicht van {{ $resource::$title }}
  @endslot

  @slot("pageroute")
  {{ $resource::$title_plural }}
  @endslot

  @slot("content")
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Hallo.
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <button class="btn btn-primary" text="Toevoegen" onclick="window.location='{{url("admin/" . $resource::$route)}}/create'">Toevoegen</button>
          </div>

        </div>
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="row"></th>
              @foreach($resource::$columns_info as $column_info)
                <td> {{ $column_info["title"] }}   </td>
              @endforeach
              <td></td>
            </tr>
          </thead>
          <tbody>
            @foreach( $items as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                @foreach(array_keys($resource::$columns_info) as $column_info)
                  <td>
                     {{ $item->$column_info }}
                  </td>
                @endforeach

                @if (!empty($resource_edit))
                  <td>
                    <a href="{{url("admin/" . $resource::$route)}}/{{$item->id}}/edit"<em class="fa fa-edit"></em>
                  </td>
                @endif
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div><!--/.row-->

  @endslot
@endcomponent
