@component("admin.layout.htmlhead")
  @slot("title")
     Inloggen in BabyCMS
  @endslot
@endcomponent

  <div class="container">
    <div class="jumbotron">
      <div class="row">
          <div class="col-md-12">

              <h1> Inloggen </h1>
              @if (auth()->check())
                <h2> HOI MAX </h2>
              @endif
          </div>
          <form method="POST" action="/admin/login">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-md-6">
              <input type="text" class="form-control" name="email" id="email" placeholder="Uw e-mail adres" />
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6">
              <input type="password" class="form-control" name="password" id="password"/>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-md-6">
                <input type="submit" class="btn btn-primary" value="Inloggen" />
                </div>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>


</body>
</html>
