@component("admin.layout.master")
  @slot("title")
    Overzicht van {{ $resource::$title_plural }}
  @endslot

  @slot("pageroute")
  {{ $resource::$title_plural }}
  @endslot

  @slot("content")
  <div class="row">
    <div class="col-md-4">
      <div class="btn btn-primary" onclick="window.location='{{url("admin/" . $resource::$route)}}/create'">
        Toevoegen
      </div>
    </div>
  </div>
  @foreach($items as $item)
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <a href="{{ url("/admin/groups/" . $item->id . "/edit") }}"<em class="fa fa-edit"></em></a>
          {{ $item->name }}

        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <i> {{ $item->description }} </i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
            <div class="row">
              <div class="col-md-4">
                <div class="panel panel-info">
                  <div class="panel-heading">Gebruikers
                    <span class="pull-right clickable panel-toggle panel-collapsed"><em class="fa fa-toggle-down"></em></span></div>
                  <div class="panel-body" style="display:none">

                      @foreach($item->users as $user)
                        <div class="row">
                          <div class="col-xs-2"><i>{{$loop->iteration}} </i></div>
                          <div class="col-xs-8"><p> {{ $user->name }} </p></div>
                          <div class="col-xs-2"><a href="{{url("admin/groups/" . $item->id . "/disconnect/" . $user->id)}}"><em class="fa fa-ban"></em></a></div>

                        </div>
                      @endforeach
                      <hr />
                      <br />
                      <b> Een gebruiker toevoegen </b>
                      <form method="POST" action="{{url("/admin/groups/connect")}}">
                        {{csrf_field() }}
                        <input type="hidden" name="group_id" value="{{$item->id}}"></input>
                        <div class="row">
                          <div class="col-md-8">
                            <select class="form-control col-md-8" name="user_id">
                              @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-md-4">
                            <input type="submit" class="btn btn-primary form-control" name="submit" value="Toevoegen" />
                          </div>
                        </div>
                      </form>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="panel panel-info">
                  <div class="panel-heading">Rechten
                    <span class="pull-right clickable panel-toggle panel-collapsed"><em class="fa fa-toggle-down"></em></span></div>
                  <div class="panel-body" style="display:none">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui pharetra molestie. Fusce et dui urna.</p>
                  </div>
                </div>
              </div>
            </div>

  @endforeach

@endslot
@endcomponent
