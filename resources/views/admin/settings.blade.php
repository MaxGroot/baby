@component("admin.layout.master")
  @slot("title")
    Instellingen
  @endslot

  @slot("pageroute")
    Instellingen
  @endslot

  @slot("content")
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
        <form role="form" method="post" action='{{ url("admin/settings") }}'>
        @foreach($settings as $setting)
          <div class="row">
              <div class="col-md-12">
                <h2> {{ $setting->name }} </h2>
                <i> {{ $setting->description }} </i> <br />
                @if ($setting->type == "string")

                  <input class="form-control" type="text" name="setting_{{$setting->id}}" value="{{$setting->value_string}}"></input>

                @elseif ($setting->type == "bool")
                <label class="switch">
                  <input class="form-control"  name="setting_{{$setting->id}}" type="checkbox" @if($setting->value_bool == 1) checked=""  @endif>
                  <span class="slider round"></span>
                </label>
                @elseif ($setting->type == "number")
                  <input class="form-control" type="number" name="setting_{{$setting->id}}" value="{{$setting->value_number}}"></input>
                @endif

              </div>
            </div>

        @endforeach

        <div class="row">
          <div class="col-md-4">
            <input class="btn btn-primary form-control" type="submit" value="Instellingen opslaan" name="submit"></input>
          </div>
        </div>
        {{ csrf_field() }}
        <input name="_method" type="hidden" value="PUT">
        </form>

      </div>
    </div>
  </div>
</div><!--/.row-->

  @endslot
@endcomponent
