@component("admin.layout.master")
  @slot("title")
    Een {{ $resource::$title }} bewerken
  @endslot

  @slot("pageroute")
    <a href="{{url("/admin/" . $resource::$route)}}">{{ $resource::$title_plural }}</a> / Bewerken
  @endslot

  @slot("content")

    <div class="panel panel-default">
      <div class="panel-heading">

        POEPJES
      </div>
      <div class="panel-body">
        <form role="form" method="post" action='{{ url("admin/" .$resource::$route) }}'>
        @foreach($resource::$columns_info as $attribute => $column_info)
          <div class="form-group">
            <label> {{ $column_info["title"] }} </label>
            @component("admin.layout.formelement." . $column_info["form_type"])
              @slot("name")
                {{ $attribute }}
              @endslot
              @slot("placeholder")

              @endslot
              @slot("value")
                {{ $item->$attribute }}
              @endslot
            @endcomponent
          </div>
        @endforeach


              <div class="row"><button type="submit" name="submit" class="col-sm-3 btn btn-primary">Bewerken </button>
              {{ csrf_field() }}
              <input name="_method" type="hidden" value="PUT">
              <input name="id" type="hidden" value="{{$item->id}}">
        </form>
        <td>
                <form action="{{ url('admin/' . $resource::$route . '/' . $item->id . '/destroy')}}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <input name="id" type="hidden" value="{{$item->id}}">
                <button type="submit" class="col-sm-3 btn btn-danger">
                  <em class="fa fa-trash"></em> Verwijderen
                </button>
              </form>
            </div>
      </div>
    </div>

  @endslot
@endcomponent
