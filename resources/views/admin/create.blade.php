@component("admin.layout.master")
  @slot("title")
    Een {{ $resource::$title }} aanmaken
  @endslot

  @slot("pageroute")
    {{ $resource::$title_plural }} / Nieuw
  @endslot

  @slot("content")
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        POEPJES
      </div>
      <div class="panel-body">
        <form role="form" method="post" action='{{ url("admin/" .$resource::$route) }}'>
        @foreach($resource::$columns_info as $attribute => $column_info)
          <div class="form-group">
            <label> {{ $column_info["title"] }} </label>
            @component("admin.layout.formelement." . $column_info["form_type"])
              @slot("name")
                {{ $attribute }}
              @endslot
              @slot("value")

              @endslot
              @slot("placeholder")
                {{ $column_info["placeholder"]}}
              @endslot
            @endcomponent
          </div>
        @endforeach
              <!-- <div class="form-group">
                <label>Naam artikel</label>
                <input class="form-control" name="title">
              </div>
              <div class="form-group">
                <label>Het nieuwsbericht:</label>
                <textarea class="form-control" rows="3" name="text"></textarea>
              </div>
              -->

              <button type="submit" name="submit" class="btn btn-primary">Aanmaken</button>
              {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
</div><!--/.row-->

  @endslot
@endcomponent
