<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/', function () {
    return view('admin.dashboard');
});

// Front-end
Route::get("/admin/news/{news_item}/show","news_controller@show");


// Back-end: News
Route::get("/admin/news/create","news_controller@create");
Route::get("/admin/news/{news_item}/edit","news_controller@edit");
Route::get("/admin/news/","news_controller@admin_index");
Route::post("/admin/news/","news_controller@store");
Route::put("/admin/news/","news_controller@update");
Route::delete("/admin/news/{news_item}/destroy","news_controller@destroy");

// Groups
Route::get("/admin/groups","GroupController@index");
Route::get("/admin/groups/create","GroupController@create");
Route::post("/admin/groups/","GroupController@store");
Route::get("/admin/groups/{group}/edit","GroupController@edit");
Route::put("/admin/groups/","GroupController@update");
Route::get("/admin/groups/{group}/disconnect/{userid}","GroupController@disconnect");
Route::post("/admin/groups/connect","GroupController@connect");

// Settings
Route::get("/admin/settings/","SettingController@index");
Route::put("/admin/settings/","SettingController@update");

// Authencication
Route::get("/admin/login","SessionController@create")->name("login");
Route::post("/admin/login","SessionController@store");
Route::get("/admin/logout","SessionController@destroy");
