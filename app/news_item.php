<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class news_item extends Model
{
    //

        protected $fillable = ['title', 'text'];

        public static $route = "news";
        public static $title = "nieuwsbericht";
        public static $title_plural = "Nieuwsberichten";

        public static $columns_info = [
          "title" => ["title" => "Titel", "form_type" => "text" , "placeholder" => "Titel van uw nieuwsbericht"],
          "text" => ["title" => "Tekst: ", "form_type" => "textarea" , "placeholder" => ""],


        ];
}
