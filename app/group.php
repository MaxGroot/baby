<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class group extends Model
{
    //
    protected $fillable = ['name', 'description'];

    public static $route = "groups";
    public static $title = "groep";
    public static $title_plural = "Groepen";

    public static $columns_info = [
      "name" => ["title" => "Titel", "form_type" => "text" , "placeholder" => "Titel van groep"],
      "description" => ["title" => "Beschrijving: ", "form_type" => "textarea" , "placeholder" => ""]
    ];

    public function users() {
        return $this->belongsToMany('App\User');

    }
}
