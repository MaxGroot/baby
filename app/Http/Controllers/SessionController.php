<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    protected $redirectTo = '/admin/login';

    public function __construct() {
      $this->middleware('guest')->except("destroy");
    }

    public function create() {
      return view("admin.login");

    }
    public function store(request $request) {

      if (auth()->attempt([
        "email" => $request["email"],
        "password" => $request["password"]
        ]))
      {
        // Succesful login

        return redirect("admin/login");


      }else {
        return back()->withErrors("Login mislukt!");

      }


    }

    public function destroy() {
      if (auth()->check()) {
        auth()->logout();
        return redirect("admin/login");
      }else {
        return "You were not logged in at all dipshit";

      }
    }
}
