<?php

namespace App\Http\Controllers;

use App\group;
use App\User;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
       $this->middleware("auth");
     }
    public function index()
    {
      $items = group::all();
      return view("admin.groups.list")->with([
          "resource" => group::class,
          "resource_edit" => true,
          "items" => $items,
          "users" => User::all()
          ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("admin.create")->with([
          "resource" => group::class
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $group = group::create($request->all());
          return redirect("admin/groups");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(group $group)
    {
      return view("admin.edit")->with([
        "resource" => group::class,
        "item" => $group
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, group $group)
    {

        $group = group::find($request->input("id"));
        $group->fill($request->all());
        $group->save();
        return redirect("/admin/groups/$group->id/edit");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(group $group)
    {
        //
    }

    public function disconnect(group $group , $userid) {

      $group->users()->detach($userid);
      return back();

    }

    public function connect(request $request) {
      $group = group::find($request->input("group_id"));
      $group->users()->attach($request->input("user_id"));
      return back();

    }
}
