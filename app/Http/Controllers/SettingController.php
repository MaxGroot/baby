<?php

namespace App\Http\Controllers;

use App\setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct() {
       $this->middleware("auth");
     }
    public function index()
    {
        $settings = setting::all();
        return view("admin.settings")->with("settings",$settings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $all_settings = setting::all();
        foreach($all_settings as $setting) {
          switch($setting->type) {
            case "string" :
              $setting->value_string = $request->input("setting_" . $setting->id);
              break;
            case "number" :
              $setting->value_number = $request->input("setting_" . $setting->id);
            break;
            case "bool" :
              $setting->value_bool = $request->has("setting_" . $setting->id) ? 1 : 0;
              break;
            default:
              return "ERROR LOADING SETTING";
              break;
          }
            $setting->save();
        }
        return redirect()->action("SettingController@index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(setting $setting)
    {
        //
    }
}
