<?php

namespace App\Http\Controllers;

use App\news_item;
use Illuminate\Http\Request;

class news_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
      $this->middleware("auth")->except("index");
    }
    public function index()
    {
        //
        $items = news_item::all();
        return view("layouts.blog.master")->with("items",$items);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_index() {
      $items = news_item::all();
      return view("admin.list")->with([
        "resource" => news_item::class,
        "resource_edit" => true,
        "items" => $items,

        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("admin.create")->with([
          "resource" => news_item::class
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $news_item = news_item::create($request->all());
        session()->flash("status_type","success");
        session()->flash("status_message","Nieuwsbericht is aangemaakt!");
        return redirect("/admin/news");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\news_item  $news_item
     * @return \Illuminate\Http\Response
     */
    public function show(news_item $news_item)
    {
        //
        return $news_item;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\news_item  $news_item
     * @return \Illuminate\Http\Response
     */
    public function edit(news_item $news_item)
    {
        return view("admin.edit")->with([
          "resource" => news_item::class,
          "item" => $news_item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\news_item  $news_item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, news_item $news_item)
    {
        //
          $news_item = news_item::find($request->input("id"));
          $news_item->fill($request->all());
          if ($news_item->save()) {
            session()->flash("status_type","success");
            session()->flash("status_message","Nieuwsbericht is aangepast!");
          }
          return back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\news_item  $news_item
     * @return \Illuminate\Http\Response
     */
    public function destroy(news_item $news_item)
    {
        $news_item->delete();
        session()->flash("status_type","warning");
        session()->flash("status_message","Nieuwsbericht is verwijderd.");
        return redirect("/admin/news");
    }
}
